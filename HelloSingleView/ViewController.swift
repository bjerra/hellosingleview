//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Anaïs Schönborg on 2018-10-24.
//  Copyright © 2018 Byonsoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func helloWorld(_ sender: Any) {
        print("hello button")
    }
}

